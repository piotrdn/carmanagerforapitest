package pl.springBoot.carManadzer.driver;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Scanner;

@Component
public class DriverManager {


    public void addNewDriver(List<Driver> allDrivers, Driver driver) {
        System.out.println("Name: ");
        Scanner scanner1 = new Scanner(System.in);
        driver.setName(scanner1.nextLine());

        System.out.println("Surname: ");
        Scanner scanner2 = new Scanner(System.in);
        driver.setSurname(scanner2.nextLine());

        System.out.println("Age: ");
        Scanner scanner3 = new Scanner(System.in);
        driver.setAge(scanner3.nextInt());

        System.out.println("City: ");
        Scanner scanner4 = new Scanner(System.in);
        driver.setCity(scanner4.nextLine());

        allDrivers.add(driver);

    }
    public List<Driver> getListOfDrivers(List<Driver> listOfDrivers) {

        for(int i = 0;i<listOfDrivers.size();i++) {
            System.out.println(listOfDrivers.get(i));
        }

        return listOfDrivers;
    }
}
