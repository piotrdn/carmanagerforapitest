package pl.springBoot.carManadzer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;



@SpringBootApplication
public class CarManadzerApplication {


	public static void main(String[] args) {
		SpringApplication.run(CarManadzerApplication.class, args);


	}

}
