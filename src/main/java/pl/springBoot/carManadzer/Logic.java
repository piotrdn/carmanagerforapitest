package pl.springBoot.carManadzer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import pl.springBoot.carManadzer.car.Car;
import pl.springBoot.carManadzer.car.CarManager;
import pl.springBoot.carManadzer.car.CarRepository;
import pl.springBoot.carManadzer.driver.Driver;
import pl.springBoot.carManadzer.driver.DriverManager;

import java.util.ArrayList;
import java.util.Currency;
import java.util.List;
import java.util.Scanner;

@Component
public class Logic {


    private CarManager carManager;
    public List<Car> carList = new ArrayList<Car>();

    DriverManager driverManager;
    List<Driver> listOfAllDrivers = new ArrayList<>();



    @Autowired
    public Logic(CarManager carManager, DriverManager driverManager) { //CarManager i driverManager wstrzyknięte przez konstruktor
        this.carManager = carManager;
        this.driverManager = driverManager;
    }

    @Autowired
    private CarRepository carRepository;



    @EventListener(ApplicationReadyEvent.class)
    public void mainLogic() {


        //CarManager carManager = new CarManager();
        //carManager = new CarManager(); //wstrzyknięto przez konstruktor
        int temp = 0;
        while(temp<2) {
            Car car = new Car();
            carManager.addNewCar(carList, car);
            carManager.addNewCarToList(carList, car);
            carManager.getCarList(carList);

            carRepository.save(car);//zapis do bazy SpringMySQLAPI.Car
//            Currency currency = new Currency(currencyname, code, midd, effectivedate);
//            currencyRepository.save(currency);

            temp++;
        }
        System.out.println("Lista wszystkich samochodów: " + carList);


        //driverManager = new DriverManager(); //wstrzyknięto przez konstruktor
        int temp2=0;
        while(temp2<2){
            Driver driver = new Driver();
            driverManager.addNewDriver(listOfAllDrivers, driver);
            driverManager.getListOfDrivers(listOfAllDrivers);

            temp2++;
        }


    }


}
