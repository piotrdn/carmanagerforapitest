package pl.springBoot.carManadzer.car;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.stereotype.Component;

import javax.annotation.processing.Generated;


@Entity
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)//IDENTITY inkrementacja o 1, AUTO-doesn't work. With the generation GenerationType.AUTO hibernate will look for the default hibernate_sequence table , so change generation to IDENTITY
    private Long id;

    private String mark;
    private String model;
    private int vin;
    private int power;


//    public Car(String mark, String model, int vin, int power) {
//        this.mark = mark;
//        this.model = model;
//        this.vin = vin;
//        this.power = power;
//    }

    public String getMark() {
        return mark;
    }

    public void setMark(String mark) {
        this.mark = mark;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getVin() {
        return vin;
    }

    public void setVin(int vin) {
        this.vin = vin;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    @Override
    public String toString() {
        return "Car{" +
                "mark='" + mark + '\'' +
                ", model='" + model + '\'' +
                ", vin=" + vin +
                ", power=" + power +
                '}';
    }
}
