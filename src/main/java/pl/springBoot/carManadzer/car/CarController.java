package pl.springBoot.carManadzer.car;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import pl.springBoot.carManadzer.Logic;


import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CarController {

    @Autowired
    private CarRepository carRepository;

    @Autowired
    private CarManager carManager;

    @Autowired
    private Logic logic;

    @GetMapping("/code/car/{position}")
    public String getAllCarsCode(@PathVariable(value = "position") int position) {
        Car carNew = logic.carList.get(position);
        String newCarString = carNew.getMark();
        return newCarString;
    }
    @GetMapping("/db/car/{position}")
    public Optional<Car> getAllCarsDB(@PathVariable(value = "position") Long position) {
            Optional<Car> car = carRepository.findById(position);
            return car;

    }
    @GetMapping("/code/carList")
    public List<Car> carListCode() {
        return logic.carList; //wyciaga z kodu
        //return carRepository.findAll(); //przez repozytorium wyciaga z bazy danych
    }
    @GetMapping("/db/carList")
    public List<Car> carListBD() {
        //return logic.carList; //wyciaga z kodu
        return carRepository.findAll(); //przez repozytorium wyciaga z bazy danych
    }
    @PostMapping("/db/addNewCar")
    public List<Car> addNewCarTo(@RequestBody Car car) {

        carRepository.save(car);
        return carRepository.findAll();
    }
    @DeleteMapping("/db/deleteCar/{position}")
    public Optional<Car> deleteCar(@PathVariable(value = "position") Long position) {
        Optional<Car> carOpt = carRepository.findById(position); //zwraca response
        carRepository.deleteById(position);
        return carOpt; //zwraca response
    }
    @PutMapping("/db/updateCar/{position}")
    public List<Car> updateCar(@RequestBody Car car , @PathVariable(value= "position") Long position) {
        carRepository.deleteById(position);
        carRepository.save(car);

        return carRepository.findAll();
    }





}
