package pl.springBoot.carManadzer.car;

import org.springframework.stereotype.Service;
import pl.springBoot.carManadzer.car.Car;

import java.util.List;
import java.util.Scanner;

@Service
public class CarManager {


    public void addNewCar(List<Car> cars, Car car) {
        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Mark: ");
        car.setMark(scanner1.next());

        Scanner scanner2 = new Scanner(System.in);
        System.out.println("Model: ");
        car.setModel(scanner2.nextLine());

        Scanner scanner3 = new Scanner(System.in);
        System.out.println("Power: ");
        car.setPower(scanner3.nextInt());

        Scanner scanner4 = new Scanner(System.in);
        System.out.println("Vin: ");
        car.setVin(scanner4.nextInt());

        //carList.add(car);
        //cars.add(car);

    }
    public void addNewCarToList(List<Car> cars, Car car) {
        cars.add(car);
    }
    public List<Car> getCarList(List<Car> cars) {

        for(int i =0;i<cars.size();i++) {
            System.out.println(cars.get(i));
        }

        return cars;
    }
}
